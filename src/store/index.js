import Vue from 'vue';
import Vuex from 'vuex';
import Query from '../model/Query';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    apis: [],
    categories: [],
    queries: [new Query(), new Query()],
  },
  mutations: {
    addApi(state, api) {
      state.apis.push(...api);
    },
    setApis(state, apis) {
      state.apis = apis;
    },
    setCategories(state, categories) {
      state.categories = categories;
    },
    setQueries(state, queries) {
      state.queries = queries;
    },
  },
  actions: {
    feelinLucky({ commit }) {
      commit('setApis', []);
      this.state.queries
        .map((query) => query.getUrlParams())
        .map((params) => fetch(`https://api.publicapis.org/random?${params}`)
          .then((response) => response.json())
          .then((response) => commit('addApi', response.entries)));
    },
    loadCategories({ commit }) {
      fetch('https://api.publicapis.org/categories')
        .then((response) => response.json())
        .then((response) => commit('setCategories', response.categories));
    },
    reset({ commit }) {
      commit('setApis', []);
      commit('setQueries', [new Query(), new Query()]);
    },
  },
});
