export default class Query {
  constructor() {
    this.title = null;
    this.description = null;
    this.auth = null;
    this.https = null;
    this.cors = null;
    this.category = null;
  }

  getQueryObject() {
    return Object
      .entries(this)
      .reduce((acc, [key, value]) => {
        // eslint-disable-next-line no-param-reassign
        if (value) acc[key] = value;
        return acc;
      }, {});
  }

  getUrlParams() {
    return Object
      .entries(this.getQueryObject())
      .reduce((acc, [key, value]) => {
        acc.push(`${encodeURIComponent(key)}=${encodeURIComponent(value)}`);
        return acc;
      }, [])
      .join('&');
  }
}
